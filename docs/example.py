from stars_not_colliding.a_star_w import a_star_w
from stars_not_colliding.graph import simple_graph_from_matrix
from stars_not_colliding.plan import Agent, Node, targets_reached
from networkx import Graph
from typing import Hashable, Optional


class MovingAgent(Agent):
    def __init__(self, idx: Hashable, position: Node, target: Node, graph: Graph):
        super().__init__(idx, position, target)
        self.graph = graph
        
    def avoid_collision(self, on_point: Node) -> Optional[Node]:
        neigh = self.graph[on_point]
        n = neigh.next()
        try:
            while n.agent:
                n.next()
        except StopIteration:
            return None
        return n
    
    def move(self):
        next_step = self.plan.next_step(self.position)
        if not next_step:
            return
        if next_step.agent:
            next_step = self.avoid_collision(next_step)
            if next_step:
                self.position.set_agent(None)
                next_step.set_agent(self)
            else:
                return
        else:
            self.position.set_agent(None)
            next_step.set_agent(self)

m = [[0, -1, -1,  0],
     [0, -1,  0,  0],
     [0,  0,  0, -1],
     [0,  0,  0,  0]]

G = simple_graph_from_matrix(m, diagonals=True)
A1 = MovingAgent(1, Node((0,0)), Node((0,3)), G)
A2 = MovingAgent(2, Node((0,3)), Node((0,0)), G)
A3 = MovingAgent(3, Node((3,3)), Node((2,0)), G)
agents = [A1, A2, A3]
P = a_star_w(G, agents)

i = 0
while(not targets_reached(agents)):
    i += 1
    for a in agents:
        a.move()
        print(i, a)