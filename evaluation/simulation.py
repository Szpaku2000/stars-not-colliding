import random
from stars_not_colliding.a_star_w import *
from stars_not_colliding.graph import *
from stars_not_colliding.plan import targets_reached
import time


def initialize_agents(position_list):
    """
    Returns list of agents
    """
    agents = []
    current_id = 0
    for params in position_list:
        agents.append(Agent(current_id, params[0], params[1]))
        current_id += 1
    return agents


def simulation(env: Environment, start, target, growth_factor, deviation_node, to_deviate):
    """
    matrix - matrix representing environment in which agents will move
    positions_list - list of tuples of starting and finish nodes eg. [(Node(0,0), Node(6,6))]
    """
    start_node = StateNode(start, State(start, env))
    target_node = StateNode(target, State(target, env))

    plan = a_star_w(env=env, start=start_node, target=target_node,  g=growth_factor, weight='weight') #, heuristic=euclidean_heuristic)

    # graph = simple_graph_from_matrix(env)
    # agents = initialize_agents(positions_list)

    print("Plan:")
    print(f"->\tPath: {plan.a_path}")
    print(f"->\tDomains:")
    for a, b in plan.deviation_domains.items():
        print(f"Collision point: {a}")
        print(f"\tNumber of nodes: {len(b[0].nodes)}")
        # print(f"\tNodes: {b[0].nodes}")

    current_node = start_node
    print(f"Start node: {current_node}")
    print(f"Next step: {plan.next_step(current_node)}")
    print("Path taken: ")
    deviation_happened = False
    counter = 1

    while current_node is not None and counter < 100:
        if isinstance(current_node, WavefrontLabelled):
            current_node = current_node.node
        print(current_node)

        if current_node.idx == deviation_node.idx and not deviation_happened:
            current_node = to_deviate
            deviation_happened = True
            print("Deviating on purpose")
        else:
            current_node = plan.next_step(current_node)
        counter += 1
        if counter == 100:
            print("Simulation is taking too long, aborting")


def put_agents_inside_matrix(agents, matrix):
    for agent in agents:
        agent_y = agent.position[0]
        agent_x = agent.position[1]
        if matrix[agent_y][agent_x] >= 0:
            matrix[agent_y][agent_x] = -2
        else:
            print("Agent cannot be placed in place of different agent or in the wall")
            raise RuntimeError
    return matrix


def print_matrix(matrix):
    for row in matrix:
        print(row)


def position_is_empty(matrix, position):
    return matrix[position.idx[0]][position.idx[1]] >= 0


def get_current_agents_positions(agents):
    current_positions = []
    for agent in agents:
        current_positions.append(agent.position)
    return current_positions
