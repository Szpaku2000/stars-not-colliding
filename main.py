import itertools as it
import time

import networkx
import stars_not_colliding
import evaluation
from evaluation.simulation import simulation
from stars_not_colliding.evaluation import *
from stars_not_colliding.plan import Node

# print("---------------------- EXPERIMENT 1 ----------------------")
# run_ex_1()
print("---------------------- EXPERIMENT 2 (2) ----------------------")
run_ex_2()
# print("---------------------- EXPERIMENT 3 ----------------------")
# run_ex_3()
# print("---------------------- EXPERIMENT 4 ----------------------")
# run_ex_4()
print("---------------------- EXPERIMENT 5 (3) ----------------------")
run_ex_5()
print("---------------------- EXPERIMENT 6 (4) ----------------------")
run_ex_6()
# print("---------------------- EXPERIMENT 7 ----------------------")
# run_ex_7()
print("---------------------- EXPERIMENT 8 (5) ----------------------")
run_ex_8()
# print("---------------------- EXPERIMENT 9 ----------------------")
# run_ex_9()
print("---------------------- EXPERIMENT 10 (1) ----------------------")
run_ex_10()
# print("---------------------- EXPERIMENT 11 ----------------------")
# run_ex_11()
#
# print("---------------------- SIMULATION 1 ----------------------")
# simulation_1()
# print("---------------------- SIMULATION 2 ----------------------")
# simulation_2()
# print("---------------------- SIMULATION 3 ----------------------")
# simulation_3()
# print("---------------------- SIMULATION 4 ----------------------")
# simulation_4()
# print("---------------------- SIMULATION 5 ----------------------")
# simulation_5()
# print("---------------------- SIMULATION 6 ----------------------")
# simulation_6()
# print("---------------------- SIMULATION 7 ----------------------")
# simulation_7()
