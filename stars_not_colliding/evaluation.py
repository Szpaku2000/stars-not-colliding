from evaluation.simulation import simulation
from stars_not_colliding.a_star_w import *
from stars_not_colliding.graph import *


def run_ex_1():
    environment = Environment((3, 3), [])
    start = ((1, 0), (1, 2))
    target = ((1, 2), (1, 0))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    # graph = generate_state_graph(environment, len(start))
    # path = a_star(graph, start_node, target_node, euclidean_heuristic)

    print("Starting A*W algorithm")
    result = a_star_w(env=environment, start=start_node, target=target_node)  # , heuristic=euclidean_heuristic)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # matrix = [[0, 0, 0],
    #           [0, 0, 0],
    #           [0, 0, 0]]
    # positions_list = [(Node((1, 0)), Node((1, 2))),
    #                   (Node((1, 2)), Node((1, 0)))]
    # growth_factor = 2
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_2():
    environment = Environment((3, 3), [])
    start = ((0, 0), (2, 2), (0, 2), (2, 0))
    target = ((2, 2), (0, 0), (2, 0), (0, 2))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    graph = generate_state_graph(environment, len(start))
    path = a_star(graph, start_node, target_node, euclidean_heuristic)
    print(f"A* path: {path}")

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # matrix = [[0, 0, 0],
    #           [0, 0, 0],
    #           [0, 0, 0]]
    # positions_list = [(Node((0, 0)), Node((2, 2))),
    #                   (Node((2, 2)), Node((0, 0))),
    #                   (Node((0, 2)), Node((2, 0))),
    #                   (Node((2, 0)), Node((0, 2)))]
    # growth_factor = 2
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_3():
    environment = Environment((5, 5), [])
    start = ((0, 0), (4, 4), (0, 4))
    target = ((4, 4), (0, 0), (4, 0))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    graph = generate_state_graph(environment, len(start))
    path = a_star(graph, start_node, target_node, euclidean_heuristic)
    print(f"A* path: {path}")

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # matrix = [[0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0]]
    # positions_list = [(Node((0, 0)), Node((4, 4))),
    #                   (Node((4, 4)), Node((0, 0))),
    #                   (Node((0, 4)), Node((4, 0))),
    #                   (Node((4, 0)), Node((0, 4)))]
    # growth_factor = 5
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_4():
    environment = Environment((5, 5), [(2, 2), (1, 2), (3, 2), (2, 1), (2, 3)])
    start = ((0, 0), (4, 4), (0, 4), (4, 0))
    target = ((4, 4), (0, 0), (4, 0), (0, 4))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    # graph = generate_state_graph(environment, len(start))
    # path = a_star(graph, start_node, target_node, euclidean_heuristic)
    # print(f"A* path: {path}")

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # matrix = [[0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0],
    #           [0, 0, 0, 0, 0]]
    # positions_list = [(Node((0, 0)), Node((4, 4))),
    #                   (Node((4, 4)), Node((0, 0))),
    #                   (Node((0, 4)), Node((4, 0))),
    #                   (Node((4, 0)), Node((0, 4)))]
    # growth_factor = 5
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_5():
    matrix = [[-1, 0, -1, -1, 0, 0, 0],
              [0, 0, 0, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, 0, 0, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [0, 0, 0, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, 0, 0]]

    environment = environment_from_matrix(matrix)

    start = ((0, 1), (8, 1), (1, 0), (7, 0))
    target = ((7, 2), (1, 2), (8, 6), (0, 6))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # positions_list = [(Node((0, 2)), Node((12, 11))),
    #                   (Node((12, 2)), Node((0, 11))),
    #                   (Node((2, 0)), Node((10, 4))),
    #                   (Node((10, 0)), Node((2, 4)))]
    # growth_factor = 3
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_6():
    matrix = [[0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 2), (0, 4), (0, 6))
    target = ((4, 6), (4, 4), (4, 2), (4, 0))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")


def run_ex_7():
    matrix = [[0, 0, 0],
              [0, 0, 0],
              [0, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 1), (0, 2), (2, 2), (2, 1), (2, 0))
    target = ((2, 2), (2, 1), (2, 0), (0, 0), (0, 1), (0, 2))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # positions_list = [(Node((0, 2)), Node((12, 11))),
    #                   (Node((12, 2)), Node((0, 11))),
    #                   (Node((2, 0)), Node((10, 4))),
    #                   (Node((10, 0)), Node((2, 4)))]
    # growth_factor = 3
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_8():
    matrix = [[0, 0, -1, 0, 0],
              [0, 0, -1, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, -1, 0, 0],
              [0, 0, -1, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 4), (4, 4), (4, 0))
    target = ((4, 4), (4, 0), (0, 0), (0, 4))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # positions_list = [(Node((0, 2)), Node((12, 11))),
    #                   (Node((12, 2)), Node((0, 11))),
    #                   (Node((2, 0)), Node((10, 4))),
    #                   (Node((10, 0)), Node((2, 4)))]
    # growth_factor = 3
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_9():
    matrix = [[0, 0, -1, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, -1, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, -1, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 4), (4, 4), (4, 0))
    target = ((4, 4), (4, 0), (0, 0), (0, 4))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
    #     # print(f"\tNodes: {b[0].nodes}")

    # positions_list = [(Node((0, 2)), Node((12, 11))),
    #                   (Node((12, 2)), Node((0, 11))),
    #                   (Node((2, 0)), Node((10, 4))),
    #                   (Node((10, 0)), Node((2, 4)))]
    # growth_factor = 3
    #
    # evaluation.simulation.simulation(matrix, positions_list, growth_factor)


def run_ex_10():
    environment = Environment((5, 5), [])
    start = ((2, 0), (2, 4))
    target = ((2, 4), (2, 0))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    graph = generate_state_graph(environment, len(start))
    path = a_star(graph, start_node, target_node, euclidean_heuristic)
    print(f"A* path: {path}")

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    # print(f"Number of deviation domains: {len(result.deviation_domains)}")
    # for a, b in result.deviation_domains.items():
    #     print(f"Collision point: {a}")
    #     print(f"\tNumber of nodes: {len(b[0].nodes)}")
        # print(f"\tNodes: {b[0].nodes}")


def run_ex_11():
    matrix = [[0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((1, 0), (2, 0))
    target = ((1, 4), (2, 4))

    start_node = StateNode(start, State(start, environment))
    target_node = StateNode(target, State(target, environment))

    graph = generate_state_graph(environment, len(start))
    path = a_star(graph, start_node, target_node, euclidean_heuristic)
    print(f"A* path: {path}")

    print("Starting A*W algorithm")
    result = a_star_w(environment, start_node, target_node)

    print("Results: ")
    print(f"Path: \n{result.a_path}")
    print(f"Number of deviation domains: {len(result.deviation_domains)}")
    for a, b in result.deviation_domains.items():
        print(f"Collision point: {a}")
        print(f"\tNumber of nodes: {len(b[0].nodes)}")
        # print(f"\tNodes: {b[0].nodes}")


def simulation_1():
    print("based on experiment 10")
    environment = Environment((5, 5), [])
    start = ((2, 0), (2, 4))
    target = ((2, 4), (2, 0))
    deviation_node = StateNode(((2, 2), (1, 3)), State(((2, 2), (1, 3)), environment))
    to_deviate = StateNode(((2, 2), (1, 2)), State(((2, 2), (1, 2)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)


def simulation_2():
    print("based on experiment 2")
    environment = Environment((3, 3), [])
    start = ((0, 0), (2, 2), (0, 2), (2, 0))
    target = ((2, 2), (0, 0), (2, 0), (0, 2))
    deviation_node = StateNode(((1, 0), (1, 2), (0, 1), (2, 1)), State(((1, 0), (1, 2), (0, 1), (2, 1)), environment))
    to_deviate = StateNode(((1, 0), (1, 1), (0, 0), (2, 2)), State(((1, 0), (1, 1), (0, 0), (2, 2)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)


def simulation_3():
    print("based on experiment 5")
    matrix = [[-1, 0, -1, -1, 0, 0, 0],
              [0, 0, 0, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, 0, 0, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, -1, -1],
              [0, 0, 0, -1, 0, -1, -1],
              [-1, 0, -1, -1, 0, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 1), (8, 1), (1, 0), (7, 0))
    target = ((7, 2), (1, 2), (8, 6), (0, 6))
    deviation_node = StateNode(((3, 1), (5, 1), (4, 2), (4, 4)), State(((3, 1), (5, 1), (4, 2), (4, 4)), environment))
    to_deviate = StateNode(((4, 1), (5, 1), (4, 3), (4, 4)), State(((4, 1), (5, 1), (4, 3), (4, 4)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)


def simulation_4():
    print("based on experiment 6")
    matrix = [[0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 2), (0, 4), (0, 6))
    target = ((4, 6), (4, 4), (4, 2), (4, 0))
    deviation_node = StateNode(((3, 4), (4, 4), (2, 3), (2, 1)), State(((3, 4), (4, 4), (2, 3), (2, 1)), environment))
    to_deviate = StateNode(((3, 4), (4, 4), (2, 2), (2, 1)), State(((3, 4), (4, 4), (2, 2), (2, 1)), environment))
    # deviation_node = StateNode(((3, 4), (4, 4), (2, 3), (2, 1)), State(((3, 4), (4, 4), (2, 3), (2, 1)), environment))
    # to_deviate = StateNode(((2, 4), (4, 4), (2, 2), (2, 1)), State(((2, 4), (4, 4), (2, 2), (2, 1)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)

def simulation_5():
    print("based on experiment 8")
    matrix = [[0, 0, -1, 0, 0],
              [0, 0, -1, 0, 0],
              [0, 0, 0, 0, 0],
              [0, 0, -1, 0, 0],
              [0, 0, -1, 0, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 4), (4, 4), (4, 0))
    target = ((4, 4), (4, 0), (0, 0), (0, 4))
    deviation_node = StateNode(((2, 1), (4, 0), (1, 0), (0, 1)), State(((2, 1), (4, 0), (1, 0), (0, 1)), environment))
    to_deviate = StateNode(((2, 2), (4, 0), (1, 0), (0, 1)), State(((2, 2), (4, 0), (1, 0), (0, 1)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)

def simulation_6():
    print("based on experiment 6")
    matrix = [[0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 2), (0, 4), (0, 6))
    target = ((4, 6), (4, 4), (4, 2), (4, 0))
    deviation_node = StateNode(((2, 2), (2, 4), (1, 4), (2, 5)), State(((2, 2), (2, 4), (1, 4), (2, 5)), environment))
    to_deviate = StateNode(((2, 1), (2, 3), (0, 4), (2, 6)), State(((2, 1), (2, 3), (0, 4), (2, 6)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)

def simulation_7():
    print("based on experiment 6")
    matrix = [[0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, -1, 0, -1, 0, -1, 0],
              [0, -1, 0, -1, 0, -1, 0]]

    environment = environment_from_matrix(matrix)
    start = ((0, 0), (0, 2), (0, 4), (0, 6))
    target = ((4, 6), (4, 4), (4, 2), (4, 0))
    # deviation_node = StateNode(((3, 4), (4, 4), (2, 3), (2, 1)), State(((3, 4), (4, 4), (2, 3), (2, 1)), environment))
    # to_deviate = StateNode(((2, 4), (4, 4), (2, 2), (1, 2)), State(((2, 4), (4, 4), (2, 2), (1, 2)), environment))
    deviation_node = StateNode(((3, 4), (4, 4), (2, 3), (2, 1)), State(((3, 4), (4, 4), (2, 3), (2, 1)), environment))
    to_deviate = StateNode(((2, 4), (4, 4), (2, 3), (2, 1)), State(((2, 4), (4, 4), (2, 3), (2, 1)), environment))
    simulation(environment, start, target, 2, deviation_node, to_deviate)
