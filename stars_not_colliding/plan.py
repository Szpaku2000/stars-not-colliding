from typing import List, Hashable, Optional, Sequence, Union
from networkx import Graph
import itertools as it


class Agent:
    """Agent class which should serve as a base class for library users wishing
    to use `a_star_w` algorithm."""

    def __init__(self, idx: Hashable, position, target):
        self.idx = idx
        self.position = position
        self.target = target
        self.plan: Optional[Plan] = None

    def __repr__(self) -> str:
        return "Agent " + str(self.idx) + " positioned at: " + str(self.position) + ", travelling to: " + str(
            self.target)

    def __hash__(self) -> int:
        return hash(self.idx)

    def id(self):
        return self.idx

    def set_plan(self, plan):
        self.plan = plan


def targets_reached(agents: Sequence[Agent]):
    for a in agents:
        if a.target != a.position:
            return False
    return True


class Environment:
    def __init__(self, dimensions, walls: List[tuple]):
        self.height = dimensions[0]
        self.width = dimensions[1]
        self.walls = walls

    def dimensions(self):
        return "(" + self.height + ", " + self.width + ")"


class State:
    def __init__(self, agents_positions: tuple, environment: Environment, diagonals: bool = False):
        # has list of agents' positions, and an environment
        self.agents = agents_positions
        self.environment = environment
        self.diagonals = diagonals

    def __repr__(self) -> str:
        return "State: " + str(self.id()) + " with dimensions: " + self.environment.dimensions()

    def id(self):
        result = []
        for agent in self.agents:
            result.append(agent.position)
        return result

    def agents_positions(self):
        positions = []
        for agent in self.agents:
            positions.append(agent.position)
        return positions

    def agent_neighbours(self, agent_position):
        possible = [(agent_position[0] - 1, agent_position[1]),
                    (agent_position[0], agent_position[1] - 1),
                    (agent_position[0] + 1, agent_position[1]),
                    (agent_position[0], agent_position[1] + 1)]
        if self.diagonals:
            possible.append((agent_position[0] - 1, agent_position[1] - 1))
            possible.append((agent_position[0] + 1, agent_position[1] + 1))
            possible.append((agent_position[0] + 1, agent_position[1] - 1))
            possible.append((agent_position[0] - 1, agent_position[1] + 1))

        to_del = []
        for pos in possible:
            if pos in self.environment.walls:
                to_del.append(pos)
            if pos[0] < 0 or pos[0] >= self.environment.height:
                to_del.append(pos)
            if pos[1] < 0 or pos[1] >= self.environment.width:
                to_del.append(pos)

        neighbours = [x for x in possible if x not in to_del]
        return neighbours

    def neighbours(self):
        agents_with_their_neighbours = []
        for pos in self.agents:
            cur_agent = self.agent_neighbours(pos)
            cur_agent.append(pos)
            agents_with_their_neighbours.append(cur_agent)

        # adjust positions that will be neighbours
        positions = list(it.product(*agents_with_their_neighbours))
        to_delete = [self.agents]
        for position in positions:
            # remove nodes where agents are in the same position
            if len(position) != len(set(position)):
                to_delete.append(position)
                continue
            # remove nodes that will result in agents swapping their position
            for i in range(len(self.agents)):
                for j in [x for x in range(len(self.agents)) if x != i]:
                    if position[j] == self.agents[i] and position not in to_delete:
                        to_delete.append(position)
                        continue

        for x in to_delete:
            positions.remove(x)
        for position in positions:
            yield position

    def print_table(self):
        pass


class StateNode:

    def __init__(self, idx: Hashable, state: State):
        self.idx = idx
        self.state = state

    def __hash__(self):
        return hash(tuple(self.idx))

    def __repr__(self) -> str:
        return "StateNode:" + str(self.idx)

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.idx == other.idx
        else:
            return self.idx == other

    def __lt__(self, other):
        return self.idx < other

    def __gt__(self, other):
        return self.idx > other

    def __len__(self):
        return len(self.idx)

    def __getitem__(self, i):
        return self.idx[i]

    def __iter__(self):
        return iter(self.idx)

    def id(self):
        return self.idx

    def set_state(self, state: Optional[State]):
        self.state = state
        # if agent:
        #     agent.position = self


class Node:
    """Node class used internally by `a_star_w`.

    ### Node in graphs

    It is recommended to use this wrapper class with functions of the library.
    Any hashable can be used as a base of the Node which is also true for nodes
    of a `networkx.Graph`. Any `networkx.Graph` can be changed to use the
    wrapper with invocation of `graph.from_networkx_to_nodes`. All other graph
    generators of submodule `graph` use this wrapper class.

    ### Node and agents

    The Node provides an additional field which can be used to track the
    location of agents. Use `set_agent` to change its value. The interpretation
    the field is left to library users.
    """

    def __init__(self, idx: Hashable):
        self.idx = idx
        self.agent: Optional[Agent] = None

    def __hash__(self):
        return hash(self.idx)

    def __repr__(self) -> str:
        return "Node:" + str(self.idx)

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.idx == other.idx
        else:
            return self.idx == other

    def __lt__(self, other):
        return self.idx < other

    def __gt__(self, other):
        return self.idx > other

    def __len__(self):
        return len(self.idx)

    def __getitem__(self, i):
        return self.idx[i]

    def __iter__(self):
        return iter(self.idx)

    def id(self):
        return self.idx

    def set_agent(self, agent: Optional[Agent]):
        self.agent = agent
        if agent:
            agent.position = self


Path = List[Node]


class Plan:
    """Core class of the `a_star_w` library.
    
    Plan is the data structure returned by an `a_star_w` algorithm function. It
    consists of a path generated by an A* algorithm which itself assumes no
    deviations and a list of deviation domains which are graphs labelled with
    wavefront expansion algorithm. To use the Plan invoke `next_step` - the
    method will recommend the next node to which an agent should move. If the
    agent left the path because of a collision with another agent it should
    enter a deviation domain for which the method is still valid.
    """

    def __init__(self, a_path: Path, deviation_domains: List[Graph]):
        self.a_path = a_path  # NOTE: Could use a better name
        self.deviation_domains = deviation_domains

    def __repr__(self) -> str:
        return "Plan with path: " + str(self.a_path) + " and " + str(len(self.deviation_domains)) + " d. domains"

    # def id(self) -> Agent:
    #     return self.idx

    def path(self) -> Path:
        """An A* generated path that is the base of the plan."""
        return self.a_path

    def deviation_domain(self, position: StateNode) -> Graph:
        """Returns a deviation domain to which a specific point (position) belongs."""
        for d in self.deviation_domains.values():
            for node in d:
                if position in node:
                    return node

    def next_step(self, from_pos: StateNode) -> Union[StateNode, None]:
        """Find the next step that should be taken from a given position
        according to the plan.
    
        ### Returns
        The method returns None when goal was reached otherwise it returns the
        next node to which an agent should move.
        
        ### Errors
        A RuntimeError is raised when agent asks a for a step from a position
        not included in the Plan.
        ---
        More information can be found in the documentation of `Plan` class.
        """
        if from_pos in self.a_path:
            if from_pos == self.a_path[-1]:
                # We are already at the end of the path
                return None
            i = self.a_path.index(from_pos)
            # if self.a_path[i + 1].agent is not None:
            #     # if next node is occupied agent waits
            #     return self.a_path[i]
            return self.a_path[i + 1]
        else:
            D = self.deviation_domain(from_pos)
            if not D:
                raise RuntimeError
            # use wavefront data stored in the deviation domain
            neigh = list(D.neighbors(from_pos))
            return min(neigh)


class WavefrontLabelled:
    """Class used internally by `a_star_w` within wavefront expansion. It is not
    meant to be used by a library user."""

    def __init__(self, node, cost) -> None:
        self.node = node
        self.cost = cost

    def __hash__(self) -> int:
        return hash(self.node)

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return (self.node == __o.node and self.cost == __o.cost)
        else:
            return self.node == __o

    def __lt__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return self.cost < __o.cost
        else:
            return self.node < __o

    def __gt__(self, __o: object) -> bool:
        if isinstance(__o, WavefrontLabelled):
            return self.cost > __o.cost
        else:
            return self.node > __o

    def __repr__(self) -> str:
        return "{" + str(self.node) + ": " + str(self.cost) + "}"

    def cost(self):
        return self.cost

    def node(self):
        return self.node
