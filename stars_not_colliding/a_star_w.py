import sys
import time
from math import sqrt
from typing import Dict, Mapping, Sequence, List, Set, Tuple
import itertools as it
import networkx as nx
from networkx import Graph
import matplotlib.pyplot as plt
import networkx

from stars_not_colliding.graph import simple_graph_from_matrix
from stars_not_colliding.plan import Agent, Plan, Node, WavefrontLabelled, Environment, State, StateNode

Path = List[Node]
StatePath = List[StateNode]


def generate_all_agents_positions(env: Environment, agents_number):
    possible_positions = list(it.product(range(env.height), range(env.width)))
    for wall in env.walls:
        possible_positions.remove(wall)

    result = set()
    perms = list(it.permutations(possible_positions, agents_number))
    for perm in perms:
        result.add(perm)

    return result


def generate_edges(g: Graph, env: Environment):
    for node in g.nodes:
        for neighbour in node.state.neighbours():
            neigh_node = StateNode(neighbour, State(neighbour, env))
            yield node, neigh_node


def generate_state_graph(env: Environment, agents_number: int, diagonals: bool = False) -> Graph:
    # Generating all agents positions
    possible_agent_positions = generate_all_agents_positions(env, agents_number)
    # Generating list of nodes
    nodes = []
    for pos in possible_agent_positions:
        nodes.append(StateNode(pos, State(pos, env, diagonals)))

    # Graph generation
    g = Graph()
    # Adding all nodes to graph
    g.add_nodes_from(nodes)

    # Connecting nodes with edges
    for edge_pair in generate_edges(g, env):
        g.add_edge(edge_pair[0], edge_pair[1], weight=1.0)
    return g


def euclidean_heuristic(a: StateNode, b: StateNode):
    result = 0
    a_agents = a.state.agents
    b_agents = b.state.agents
    if len(a_agents) != len(b_agents):
        raise RuntimeError
    for i in range(len(a_agents)):
        result += sqrt((a_agents[i][0] - b_agents[i][0]) ** 2 + (a_agents[i][1] - b_agents[i][1]) ** 2)
    result = round(result / len(a.state.agents), 2)
    return result


def create_paths(combined_paths: List[StateNode]):
    # possibly wrong
    paths = list()

    for i in range(len(combined_paths[0])):
        paths.append([])
        for node in combined_paths:
            paths[i].append(node[i])

    return paths


def graph_from_environment(env: Environment):
    matrix = []
    for y in range(env.height):
        matrix.append([])
        for x in range(env.width):
            if (y, x) in env.walls:
                matrix[y].append(-1)
            else:
                matrix[y].append(0)
    return simple_graph_from_matrix(matrix)


def agents_from_paths(paths):
    agents = {}
    for path in paths:
        agents[Agent(path[0], path[0], path[-1])] = path
    return agents


def a_star_w(env: Environment, start: StateNode, target: StateNode, heuristic=euclidean_heuristic,
             weight='weight', g: int = 2) -> Dict[
    Agent, Plan]:
    """Performs an A*W hybrid planning algorithm for a given graph and a list of
    agents.
    
    Creates a plan for each agent assuming all agents exist within the
    same graph. It can operate on any subclass of `networkx.Graph`. The
    algorithm offers a compromise between wavefront's robustness and low
    complexity of A*. 
    """

    # A* for all agents
    t0 = time.time()
    state_graph = generate_state_graph(env, len(start))
    t1 = time.time()
    print(f"Graf generation took: {round(t1 - t0, 3)}")
    t0 = time.time()
    path = a_star(state_graph, start, target, euclidean_heuristic)
    t1 = time.time()
    print(f"A* took: {round(t1 - t0, 3)}")

    t2 = time.time()
    agent_paths = create_paths(path)
    agents = agents_from_paths(agent_paths)
    # check collisions
    collision_points = check_for_collisions(path, g)

    # grow deviation domain
    domains: Dict[Agent, List[Graph]] = dict()
    for a_list in collision_points.values():
        for a in a_list:
            domains[a] = list()
    for cp in collision_points.keys():
        for a in collision_points[cp]:
            domains[a].append(generate_sub_space(state_graph, cp, g))

    # for cp, subgraphs in domains.items():
    #     print("Before wavefront")
    #     print(f"Collision point: {cp}")
    #     for graph in subgraphs:
    #         print(f"Subgraph: {nx.graphviews.generic_graph_view(graph)}")
    #         print(f"Nodes: {graph.nodes}")
            # nodes are StateNodes

    # calculate Wavefront and modify the domain
    for a, d_list in domains.items():
        for i in range(len(d_list)):
            d = domains[a][i]
            domains[a][i] = wavefront(d, state_graph, path, None, weight)

    # TODO delete or log in smarter way
    # for cp, subgraphs in domains.items():
    #     print("After wavefront")
    #     print(f"Collision point: {cp}")
    #     for graph in subgraphs:
    #         print(f"Subgraph: {nx.graphviews.generic_graph_view(graph)}")
    #         print(f"Nodes: {graph.nodes}")

    t3 = time.time()
    print(f"Wavefront took: {round(t3 - t2, 3)}")
    print(f"A*W took: {round(t3 - t0, 3)}")
    # at this point we should have:
    #   * paths[Agent, Path]
    #   * domains[Agent, List[Graph]]
    # now a plan should be generated

    plan = Plan(path, domains)

    return plan


def a_star(g: Graph, start: StateNode, target: StateNode, heuristic=euclidean_heuristic) -> StatePath:
    """Wrapper of `networkx.astar_path` used in `a_star_w`"""
    astar_path = nx.astar_path(g, start, target, heuristic)
    path = list()
    for n in astar_path:
        path.append(n)
    return path


def wavefront(D: Graph, G: Graph, path: StatePath, heuristic=None, weight='weight'):
    """Wavefront expansion algorithm which relabels nodes of deviation domain with
    cost. Used in `a_star_w`.

    Performs the wavefront expansion relabeling the nodes within the domain so
    that they contain the information about the cost of reaching the goal
    (the closest next point on path that is outside the domain). Since the goal does
    not exist within the deviation domain (in most cases) the access the
    original graph is supplemented with `G` argument, while the domain is passed
    as `D`.
    """
    search_path = path.copy()

    # search for a target scanning the path, starting from last element, for a
    # node closest to analyzed deviation domain. Note the node that was before
    # our match
    dest = search_path.pop()
    prev = dest
    while search_path and dest not in D.nodes:
        prev = dest
        dest = search_path.pop()

    labelled = dict()

    # If `prev != dest` (prev equal dest implies that the goal is within the
    # domain) Start with neighbours of prev that exist within the domain,
    # otherwise start with dest.
    if prev != dest:
        neigh = set(D.nodes).intersection({n for n in G.neighbors(prev)})
    else:
        neigh = D.neighbors(dest)
        labelled[dest] = 0.0

    to_visit = list()  # BFS queue

    # Start expansion with initial set. This part can be done in more performant
    # manner and thus is separated from normal BFS algorithm.
    for n in neigh:
        # Use the heuristic function, if it is available, to calculate costs.
        if heuristic:
            cost_label = heuristic(prev, n)
        else:
            cost_label = G[prev][n][weight]
        # Relabel nodes to cost
        labelled[n] = cost_label
        to_visit.append(n)

    # BFS in two versions
    # BFS do not use heuristics, doesn't it?
    # TODO possibly delete
    if not heuristic:
        while to_visit:
            currently_inspected = to_visit.pop()
            for n in D.neighbors(currently_inspected):
                if n not in labelled:
                    cost_label = G[currently_inspected][n][weight] + labelled[currently_inspected]
                    labelled[n] = cost_label
                    to_visit.append(n)
                if labelled[n] > G[currently_inspected][n][weight] + labelled[currently_inspected]:
                    labelled[n] = G[currently_inspected][n][weight] + labelled[currently_inspected]
    else:
        while to_visit:
            currently_inspected = to_visit.pop()
            for n in D.neighbors(currently_inspected):
                if n not in labelled:
                    cost_label = heuristic(prev, n)
                    labelled[n] = cost_label
                    to_visit.append(n)

    mapping = dict()
    for k, v in labelled.items():  # k = node, v = cost
        mapping[k] = WavefrontLabelled(k, v)

    D = nx.relabel_nodes(D, mapping, copy=True)
    return D


def check_for_collisions(path: List[StateNode], g: int):
    """
    Find among paths collision points.
    """

    collisions: Dict[StateNode, list] = dict()   # step, pair

    for state in path:
        for a, b in it.combinations(state.idx, 2):
            if euclidean_distance(a, b) < g:
                try:
                    collisions[state].append((a, b))
                except KeyError:
                    collisions[state] = list()
                    collisions[state].append((a, b))

    return collisions


def generate_sub_space(G: Graph, collision_point, g: int = 2) -> Graph:
    """
    Generate a deviation sub-space for a given collision point using a growth
    factor g.
    """
    sub_space_nodes = set()
    sub_space_nodes.add(collision_point)

    queue = list()
    queue.append(collision_point)

    while queue and g:
        level_size = len(queue)
        while level_size:
            level_size -= 1
            currently_inspected = queue.pop()
            neighbors = {n for n in G.neighbors(currently_inspected)}
            queue.extend(neighbors)
            sub_space_nodes = sub_space_nodes.union(neighbors)
        g -= 1

    sub_space = G.subgraph(sub_space_nodes).copy()

    return sub_space


def euclidean_distance(a, b) -> float:
    """
    A simple euclidean distance heuristic. It assumes that nodes can be
    decomposed into a tuple of coordinates, otherwise it throws IndexError.
    """
    if len(a) < 2 or len(b) < 2:
        raise IndexError
    (x1, y1) = a
    (x2, y2) = b
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
