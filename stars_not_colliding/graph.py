from copy import copy
from typing import List

from networkx import Graph
from networkx.utils import pairwise
from networkx import relabel_nodes

from stars_not_colliding.plan import Node, Environment


def allow_diagonal_moves_on_grid_graph(G: Graph, x_len: int, y_len: int) -> Graph:
    """ Adds new edges that correspond to diagonal moves on a grid assuming that
    provided graph was created with `grid_graph`. The weight of the added edges
    is equal 1.414"""

    G.add_edges_from([
                         (Node((x, y)), Node((x + 1, y + 1)))
                         for x in range(x_len - 1)
                         for y in range(y_len - 1)
                     ] + [
                         (Node((x + 1, y)), Node((x, y + 1)))
                         for x in range(x_len - 1)
                         for y in range(y_len - 1)
                     ], weight=1.414)
    return G


def grid_graph(x_len, y_len) -> Graph:
    """Works similarly to `networkx.generators.lattice.grid_2d_graph` but uses
    the `Node` class."""

    G = Graph()
    rows = range(x_len)
    cols = range(y_len)
    G.add_nodes_from(Node((i, j)) for i in rows for j in cols)
    G.add_edges_from((Node((i, j)), Node((pi, j)), {'weight': 1.0}) for pi, i in pairwise(rows) for j in cols)
    G.add_edges_from((Node((i, j)), Node((i, pj)), {'weight': 1.0}) for i in rows for pj, j in pairwise(cols))
    return G


def grid_graph_with_diagonals(x_len: int, y_len: int) -> Graph:
    """Uses `grid_graph` and runs `allow_diagonal_moves_on_grid_graph` on the
    graph."""

    G = grid_graph(x_len, y_len)
    G = allow_diagonal_moves_on_grid_graph(G, x_len, y_len)
    return G


def simple_graph_from_matrix(matrix: List[List[int]], diagonals: bool = False) -> Graph:
    """Allows creation of a graph using a matrix.

    ### Diagonals
    To create a graph allowing diagonal moves on the grid set `diagonals=True`.

    ### Matrix format
    Negative numbers imply tiles which cannot be reached. Length of a first row is used to
    detrmine the number of columns in the whole matrix. If any other row is
    shorter, an error will be thrown.
    """
    rows = len(matrix)
    cols = len(matrix[0])

    if diagonals:
        G = grid_graph_with_diagonals(rows, cols)
    else:
        G = grid_graph(rows, cols)

    for r in range(rows):
        for c in range(cols):
            if matrix[r][c] < 0:
                G.remove_node(Node((r, c)))
    return G


def from_networkx_to_nodes(G: Graph) -> Graph:
    edges = copy(G.edges)
    mapping = dict()
    for node in sorted(G):
        mapping[node] = Node(node)
    G = relabel_nodes(G, mapping)
    return G


def environment_from_matrix(matrix: List):
    walls = []
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == -1:
                walls.append((i, j))

    return Environment((len(matrix), len(matrix[i])), walls)
