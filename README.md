# stars-not-colliding
An implementation of a multi-agent pathfinding algorithm that uses A-star/Wavefront hybrid approach to avoid collisions or deviations with minimal computational complexity.

\*-\*-\*-\*-\*

# Documentation

A user manual can be found at [`docs/user_manual.md`](docs/user_manual.md). Additionally, most elements of the library have been documented with docstrings and extra examples can be found in [`stars_not_colliding/tests.py`](stars_not_colliding/tests.py). 

# Build notes

## To get a distributable package

Make sure to have PyPa build tool installed:
```sh
pip install build
```
Then run:
```sh
python -m build
```

## To get a development friendly package
To get a real time updated package run:
```sh
pip install --editable .
```
Might require root access.

## Running tests
```sh
python stars_not_colliding/tests.py
```